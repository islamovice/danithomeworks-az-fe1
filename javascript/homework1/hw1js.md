# 1.Difference between declaring variables var, let and const?

let allows you to declare variables that are limited in scope. unlike the var keyword, 
which defines a variable globally, or locally to an entire function regardless of block scope.
Like let declarations, const can only be accessed within the scopes it was declared,
but it can't be updated or re-declared.

# 2.Why is declaration of a variable via var considered a bad tone?

Var can redefine the global variable inside the scopes.