function interview() {
  let name = prompt("Whats your name?");
  if (name == null) {
    alert("You didn't enter the name");
    return;
  }

  let age = parseInt(prompt("How old are you?"));
  if (age == null ) {
    alert("You didn't enter the age");
    return;
  }

  if (isNaN(age)) {
    alert("You didn't enter the number")
  }

  if (!isNaN(name)) {
    alert("You put the wrong name");
    return;
  }

  if (age < 18 && age > 0) {
    alert("You are not allowed to visit this website");
    return;
  }

  if (age > 22) {
    alert("Welcome, " + name);
    return;
  }

  if (age >= 18 && age <= 22) {
    let conf = confirm("Are you sure you want to continue?");
    if (conf === true)
      alert("Welcome, " + name);
    else
      alert("You are not allowed to visit this website");
  }
}
interview();
